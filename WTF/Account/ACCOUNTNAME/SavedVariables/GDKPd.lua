
GDKPd_DB = {
	["global"] = {
		["shownPopupAddonMsg4_2"] = true,
	},
	["profileKeys"] = {
		["Vraka - Venoxis"] = "physisch-gdkp",
		["Kreyah - Venoxis"] = "physisch-gdkp",
		["Varotan - Venoxis"] = "physisch-gdkp",
		["Anveera - Venoxis"] = "physisch-gdkp",
		["Veyah - Venoxis"] = "physisch-gdkp",
	},
	["profiles"] = {
		["physisch-gdkp"] = {
			["balancepoint"] = {
				["y"] = -152.000198364258,
				["relative"] = "RIGHT",
				["point"] = "RIGHT",
				["x"] = -152.499664306641,
			},
			["rulesString"] = "Regeln zum Raidablauf zu finden im #infos-regeln channel im discord. Wichtig Sind hierbei: [Gold-DKP Regeln] & [\"Raid\" Spezifische Regeln]",
			["auctionTimer"] = 25,
			["movable"] = false,
			["anchorBalance"] = true,
			["point"] = {
				["y"] = -196.999572753906,
				["relative"] = "TOPRIGHT",
				["point"] = "TOPRIGHT",
				["x"] = -186.999450683594,
			},
			["auctionTimerRefresh"] = 20,
			["hideCombat"] = {
				["status"] = false,
				["vercheck"] = false,
			},
			["confirmMail"] = true,
			["hide"] = false,
			["startBid"] = 500,
			["increment"] = 50,
			["showAuctionDurationTimerText"] = true,
			["announceBidRaidWarning"] = true,
			["statuspoint"] = {
				["y"] = -90.170539855957,
				["x"] = 74.9558258056641,
				["point"] = "TOPLEFT",
				["relative"] = "TOPLEFT",
			},
			["filteredSettings"] = {
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "16806 16804 16828 16830",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 10,
					["enable"] = true,
					["increment"] = 5,
					["setIncrement"] = true,
				}, -- [1]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "18203 17110 17082 17074",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 10,
					["enable"] = true,
					["increment"] = 5,
					["setIncrement"] = true,
				}, -- [2]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "16795 16796 16797 16798 16799 16800 16801 16802 16803 16804 16805 16806 16807 16808 16809 16810 16811 16812 16813 16814 16815 16816 16817 16819 16820 16821 16822 16823 16824 16826 16829 16831 16833 16834 16835 16836 16837 16838 16839 16840 16841 16842 16843 16844 16845 16846 16847 16848 16849 16850 16851 16852 16853 16854 16855 16856 16857 16858 16859 16860 16861 16862 16863 16864 16865 16866 16867 16868 17065 17071 17072 17073 17077 17107 18803 18806 18808 18809 18811 18812 18815 18824 18829 18861 18870 18872 18878 18879 19138 19139 19144 19147",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 30,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [3]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleName"] = "itemid",
							["ruleArgs"] = {
								["name"] = "",
								["idlist"] = "16901 16909 16915 16922 16930 16938 16946 16954 16962 17066 17102 17106 17109 18252 18257 18259 18260 18264 18265 18290 18291 18292 18646 18703 18810 18817 18822 18823 18875 19142 19143 19145 21371 ",
							},
						}, -- [1]
					},
					["startBid"] = 50,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [4]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "16954 16962 17063 17069 17076 17103 17104 17105 18805 18814 18816 18820 18821 18832 18842 19136 19137 19140 19147 ",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 100,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [5]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "17204",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 300,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [6]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "16818 16832 16897 16898 16899 16902 16903 16904 16905 16906 16907 16910 16911 16912 16913 16916 16917 16918 16919 16920 16923 16924 16925 16926 16927 16928 16931 16932 16933 16934 16935 16936 16937 16940 16941 16942 16943 16944 16945 16948 16949 16950 16951 16952 16953 16956 16957 16958 16959 16960 16961 16964 16965 16966 19335 19336 19337 19340 19341 19342 19344 19345 19350 19354 19357 19358 19369 19371 19373 19376 19386 19388 19389 19390 19391 19393 19396 19399 19401 19405 19407 19433 19435 19439 20383 ",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 50,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [7]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleName"] = "itemid",
							["ruleArgs"] = {
								["idlist"] = "13967 19002 19003 19334 19339 19346 19347 19348 19351 19353 19355 19362 19365 19368 19370 19372 19374 19378 19380 19381 19394 19397 19398 19400 19430 19431 19432 19434 19436 19437 19438 ",
							},
						}, -- [1]
					},
					["startBid"] = 100,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [8]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleName"] = "itemid",
							["ruleArgs"] = {
								["idlist"] = "19349 19352 19356 19360 19361 19364 19375 19377 19379 19382 19385 19387 19395 19403 19406 ",
							},
						}, -- [1]
					},
					["startBid"] = 300,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [9]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleName"] = "itemid",
							["ruleArgs"] = {
								["idlist"] = "20726 20727 20728 20729 20730 20731 20734 20735 21607 21611 21616 21617 21618 21624 21626 21627 21635 21648 21652 21665 21671 21675 21678 21680 21684 21685 21687 21689 21691 21692 21696 21697 21698 21699 21700 21702 21703 21705 21706 21707 21708 21856 21888 23558",
							},
						}, -- [1]
					},
					["startBid"] = 50,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [10]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "20926 20927 20928 20929 20930 20931 20932 20933 21128 21321 21579 21596 21598 21599 21600 21601 21605 21609 21619 21621 21625 21639 21645 21647 21651 21664 21668 21669 21673 21674 21676 21677 21679 21681 21682 21690 21693 21694 21837 21838 21891 22396 22399 ",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 150,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [11]
				{
					["setStartBid"] = true,
					["rules"] = {
						{
							["ruleArgs"] = {
								["idlist"] = "21814 21701 21237 21232 21690 21603 21688 21686 21672 21670 21666 21663 21650 21622 21620 21602 21597 21608 21604 21615 23570 21610 23557 22732 21583 22731 22730 21582 21586 21585 21581 21839 21126 21221 21836 ",
							},
							["ruleName"] = "itemid",
						}, -- [1]
					},
					["startBid"] = 300,
					["enable"] = true,
					["increment"] = 50,
					["setIncrement"] = true,
				}, -- [12]
			},
		},
	},
}
GDKPd_PotData = {
	["playerBalance"] = {
		["Stichtag"] = -180,
		["Abrakadaniel"] = -750,
		["Kabumm"] = -80,
		["Toetikus"] = -2000,
		["Legalize"] = -840,
		["Booztyaa"] = -150,
		["Botinka"] = -150,
		["Bakagaijin"] = -78,
		["Lù"] = -4150,
		["Fipps"] = -700,
		["Kriegihna"] = -2550,
		["Nakazz"] = -130,
		["Anaemic"] = -1130,
		["Shockzone"] = -150,
		["Twistor"] = -301,
		["Garrok"] = -320,
		["Uncleacid"] = -547,
		["Promz"] = -1700,
		["Bloodinc"] = -2000,
		["Arbraxon"] = -1450,
		["Veyah"] = -3450,
		["Kleinerdolch"] = -298,
	},
	["curPotHistory"] = {
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:21128::::::::60:::::::|h[Staff of the Qiraji Prophets]|h|r",
			["name"] = "Booztyaa",
		}, -- [1]
		{
			["bid"] = 200,
			["item"] = "|cff0070dd|Hitem:21321::::::::60:::::::|h[Red Qiraji Resonating Crystal]|h|r",
			["name"] = "Legalize",
		}, -- [2]
		{
			["bid"] = 50,
			["item"] = "|cffa335ee|Hitem:21695::::::::60:::::::|h[Angelista's Touch]|h|r",
			["name"] = "Kabumm",
		}, -- [3]
		{
			["bid"] = 50,
			["item"] = "|cffa335ee|Hitem:21691::::::::60:::::::|h[Ooze-ridden Gauntlets]|h|r",
			["name"] = "Kriegihna",
		}, -- [4]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:21676::::::::60:::::::|h[Leggings of the Festering Swarm]|h|r",
			["name"] = "Anaemic",
		}, -- [5]
		{
			["bid"] = 1700,
			["item"] = "|cffa335ee|Hitem:21663::::::::60:::::::|h[Robes of the Guardian Saint]|h|r",
			["name"] = "Veyah",
		}, -- [6]
		{
			["bid"] = 1300,
			["item"] = "|cffa335ee|Hitem:20928::::::::60:::::::|h[Qiraji Bindings of Command]|h|r",
			["name"] = "Promz",
		}, -- [7]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:20932::::::::60:::::::|h[Qiraji Bindings of Dominance]|h|r",
			["name"] = "Shockzone",
		}, -- [8]
		{
			["bid"] = 1750,
			["item"] = "|cffa335ee|Hitem:21620::::::::60:::::::|h[Ring of the Martyr]|h|r",
			["name"] = "Veyah",
		}, -- [9]
		{
			["bid"] = 2000,
			["item"] = "|cffa335ee|Hitem:21620::::::::60:::::::|h[Ring of the Martyr]|h|r",
			["name"] = "Toetikus",
		}, -- [10]
		{
			["bid"] = 250,
			["item"] = "|cffa335ee|Hitem:20930::::::::60:::::::|h[Vek'lor's Diadem]|h|r",
			["name"] = "Lù",
		}, -- [11]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:21601::::::::60:::::::|h[Ring of Emperor Vek'lor]|h|r",
			["name"] = "Arbraxon",
		}, -- [12]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:21605::::::::60:::::::|h[Gloves of the Hidden Temple]|h|r",
			["name"] = "Arbraxon",
		}, -- [13]
		{
			["bid"] = 400,
			["item"] = "|cffa335ee|Hitem:20926::::::::60:::::::|h[Vek'nilash's Circlet]|h|r",
			["name"] = "Promz",
		}, -- [14]
		{
			["bid"] = 2100,
			["item"] = "|cffa335ee|Hitem:23557::::::::60:::::::|h[Larvae of the Great Worm]|h|r",
			["name"] = "Lù",
		}, -- [15]
		{
			["bid"] = 750,
			["item"] = "|cffa335ee|Hitem:22731::::::::60:::::::|h[Cloak of the Devoured]|h|r",
			["name"] = "Abrakadaniel",
		}, -- [16]
		{
			["bid"] = 1000,
			["item"] = "|cffa335ee|Hitem:22732::::::::60:::::::|h[Mark of C'Thun]|h|r",
			["name"] = "Arbraxon",
		}, -- [17]
		{
			["bid"] = 1800,
			["item"] = "|cffa335ee|Hitem:21221::::::::60:::::::|h[Eye of C'Thun]|h|r",
			["name"] = "Lù",
		}, -- [18]
		{
			["bid"] = 2500,
			["item"] = "|cffa335ee|Hitem:20929::::::::60:::::::|h[Carapace of the Old God]|h|r",
			["name"] = "Kriegihna",
		}, -- [19]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:20933::::::::60:::::::|h[Husk of the Old God]|h|r",
			["name"] = "Botinka",
		}, -- [20]
		{
			["bid"] = 2000,
			["item"] = "|cffa335ee|Hitem:20927::::::::60:::::::|h[Ouro's Intact Hide]|h|r",
			["name"] = "Bloodinc",
		}, -- [21]
		{
			["bid"] = 150,
			["item"] = "|cffa335ee|Hitem:20931::::::::60:::::::|h[Skin of the Great Sandworm]|h|r",
			["name"] = "Arbraxon",
		}, -- [22]
		{
			["bid"] = 80,
			["item"] = "|cff0070dd|Hitem:20876::::::::60:::::::|h[Idol of Death]|h|r",
			["name"] = "Anaemic",
		}, -- [23]
		{
			["bid"] = 310,
			["item"] = "|cff0070dd|Hitem:20875::::::::60:::::::|h[Idol of Night]|h|r",
			["name"] = "Legalize",
		}, -- [24]
		{
			["bid"] = 330,
			["item"] = "|cff0070dd|Hitem:20875::::::::60:::::::|h[Idol of Night]|h|r",
			["name"] = "Legalize",
		}, -- [25]
		{
			["bid"] = 320,
			["item"] = "|cff0070dd|Hitem:20875::::::::60:::::::|h[Idol of Night]|h|r",
			["name"] = "Garrok",
		}, -- [26]
		{
			["bid"] = 50,
			["item"] = "|cff0070dd|Hitem:20881::::::::60:::::::|h[Idol of Strife]|h|r",
			["name"] = "Kleinerdolch",
		}, -- [27]
		{
			["bid"] = 50,
			["item"] = "|cff0070dd|Hitem:20881::::::::60:::::::|h[Idol of Strife]|h|r",
			["name"] = "Nakazz",
		}, -- [28]
		{
			["bid"] = 40,
			["item"] = "|cff0070dd|Hitem:20881::::::::60:::::::|h[Idol of Strife]|h|r",
			["name"] = "Nakazz",
		}, -- [29]
		{
			["bid"] = 40,
			["item"] = "|cff0070dd|Hitem:20881::::::::60:::::::|h[Idol of Strife]|h|r",
			["name"] = "Nakazz",
		}, -- [30]
		{
			["bid"] = 90,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Stichtag",
		}, -- [31]
		{
			["bid"] = 90,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Stichtag",
		}, -- [32]
		{
			["bid"] = 85,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Kleinerdolch",
		}, -- [33]
		{
			["bid"] = 85,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Kleinerdolch",
		}, -- [34]
		{
			["bid"] = 76,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Uncleacid",
		}, -- [35]
		{
			["bid"] = 78,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Bakagaijin",
		}, -- [36]
		{
			["bid"] = 78,
			["item"] = "|cff0070dd|Hitem:20882::::::::60:::::::|h[Idol of War]|h|r",
			["name"] = "Kleinerdolch",
		}, -- [37]
		{
			["bid"] = 30,
			["item"] = "|cff0070dd|Hitem:20877::::::::60:::::::|h[Idol of the Sage]|h|r",
			["name"] = "Anaemic",
		}, -- [38]
		{
			["bid"] = 30,
			["item"] = "|cff0070dd|Hitem:20877::::::::60:::::::|h[Idol of the Sage]|h|r",
			["name"] = "Uncleacid",
		}, -- [39]
		{
			["bid"] = 30,
			["item"] = "|cff0070dd|Hitem:20877::::::::60:::::::|h[Idol of the Sage]|h|r",
			["name"] = "Kabumm",
		}, -- [40]
		{
			["bid"] = 425,
			["item"] = "|cff0070dd|Hitem:20874::::::::60:::::::|h[Idol of the Sun]|h|r",
			["name"] = "Fipps",
		}, -- [41]
		{
			["bid"] = 570,
			["item"] = "|cff0070dd|Hitem:20874::::::::60:::::::|h[Idol of the Sun]|h|r",
			["name"] = "Anaemic",
		}, -- [42]
		{
			["bid"] = 441,
			["item"] = "|cff0070dd|Hitem:20874::::::::60:::::::|h[Idol of the Sun]|h|r",
			["name"] = "Uncleacid",
		}, -- [43]
		{
			["bid"] = 300,
			["item"] = "|cffffffff|Hitem:21762::::::::60:::::::|h[Greater Scarab Coffer Key]|h|r",
			["name"] = "Anaemic",
		}, -- [44]
		{
			["bid"] = 275,
			["item"] = "|cffffffff|Hitem:21762::::::::60:::::::|h[Greater Scarab Coffer Key]|h|r",
			["name"] = "Fipps",
		}, -- [45]
		{
			["bid"] = 301,
			["item"] = "|cffffffff|Hitem:21762::::::::60:::::::|h[Greater Scarab Coffer Key]|h|r",
			["name"] = "Twistor",
		}, -- [46]
	},
	["history"] = {
	},
	["potAmount"] = 23104,
	["prevDist"] = 0,
}
GDKPd_BalanceData = {
}
