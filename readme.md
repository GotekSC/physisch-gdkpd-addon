# GDKPd settings for \<Physisch Anwesend\>
***
Addon and settings for the GDKPd addon to automate the auctioning process in Gold-DKP Raids with \<Physisch Anwesend\> on Venoxis EU

## Installation
***
1. Out-of-game Setup
 - If you're paranoid, make a copy/backup of the following directories:
```
/World of Warcraft/_classic_/Interface
/World of Warcraft/_classic_/WTF
```
 - Among the files you downloaded, go into `WTF/Account` and rename `ACCOUNTNAME` to the one you find in your own wow installation under: `/World of Warcraft/_classic_/WTF/Account/` (This could be a string of numbers, or a real name if you have an old boomer wow account.
 - Copy the `Interface` and `WTF` folder in your `World of Warcraft/_classic_/` directory.

3. Ingame Setup
 - Open the GDKPd settings menu with `/gdkpd` and change the profile to `physisch-gdkp`
<p align="center">
  <img width="700px" src=".img/screen-profile.jpg" />
</p>
 - To get rid of the white message, lock the anchor when you're satisfied with the position. While being Lootmaster, the addon will display it's interface there.
<p align="center">
  <img width="257px" src=".img/screen-anchor.jpg" />
</p>
<p align="center">
  <img width="700px" src=".img/screen-lock.jpg" />
</p>

4. Auctioning Items
 - The only thing you need to do to start the Auction as Lootmaster is typing `/gdkpd auction [post-item-with-shift-click-here]`
